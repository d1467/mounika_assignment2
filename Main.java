import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {

		Employee e1 = new Employee();
		Employee e2 = new Employee();
		Employee e3 = new Employee();
		Employee e4 = new Employee();
		Employee e5 = new Employee();
		
		System.out.println("List of employees");
		e1.setId(1);
		e1.setName("Aman ");
		e1.setAge(20);
		e1.setSalary(1100000);
		e1.setDepartment("IT");
		e1.setCity("Delhi");

		e2.setId(2);
		e2.setName("Bobby");
		e2.setAge(22);
		e2.setSalary(500000);
		e2.setDepartment("Hr");
		e2.setCity("Bombay");

		
		e3.setId(3);
		e3.setName("Zoe ");
		e3.setAge(20);
		e3.setSalary(750000);
		e3.setDepartment("Admin ");
		e3.setCity("Delhi");

			
		e4.setId(4);
		e4.setName("Smitha ");
		e4.setAge(21);
		e4.setSalary(1000000 );
		e4.setDepartment("IT");
		e4.setCity("Chennai");

		
		e5.setId(5);
		e5.setName("Smitha ");
		e5.setAge(24 );
		e5.setSalary(12000000);
		e5.setDepartment("Hr");
		e5.setCity("Bengaluru");

		

		e1.EmployeDetails();
		e2.EmployeDetails();
		e3.EmployeDetails();
		e4.EmployeDetails();
		e5.EmployeDetails();
		
		
		System.out.println();
		System.out.println("Names of all employees in the sorted order are:");

		ArrayList<Employee> employeedata = new ArrayList<Employee>();
		employeedata.add(e1);
		employeedata.add(e2);
		employeedata.add(e3);
		employeedata.add(e4);
		employeedata.add(e5);
		
		

		Employeenamesort empname = new Employeenamesort();
		empname.sortingNames(employeedata);

		System.out.println();
		System.out.println("Count of Employees from each city:");


		City cityname = new City();
		cityname.CityCount(employeedata);

		System.out.println();
		System.out.println("Monthly Salary of employee along with their ID is:");

		Employeesalary empsalary = new Employeesalary();
		empsalary.salary(employeedata);

	}

}

